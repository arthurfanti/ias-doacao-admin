'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_init = function() {
			$(document).foundation();
            // needed to use joyride
            // doc: http://foundation.zurb.com/docs/components/joyride.html
            $(document).on('click', '#start-jr', function () {
                $(document).foundation('joyride', 'start');
            });
            $(document).ready( function () {
			    $('#datatable').DataTable();
			});
			$(document).foundation({
			  offcanvas : {
			    // Sets method in which offcanvas opens.
			    // [ move | overlap_single | overlap ]
			    open_method: 'overlap', 
			    // Should the menu close when a menu link is clicked?
			    // [ true | false ]
			    close_on_click : true
			  }
			});			
			_userAgentInit();
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();
